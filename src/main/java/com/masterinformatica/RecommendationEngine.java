package com.masterinformatica;

import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.python.DoubleArrayToWritableConverter;
import org.apache.spark.mllib.classification.SVMModel;
import org.apache.spark.mllib.classification.SVMWithSGD;
import org.apache.spark.mllib.clustering.KMeans;
import org.apache.spark.mllib.clustering.KMeansModel;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.mllib.stat.MultivariateStatisticalSummary;
import org.apache.spark.mllib.stat.Statistics;
import org.apache.spark.mllib.util.MLUtils;
import scala.Tuple2;
import scala.Tuple3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Alejandro Pérez Vereda
 *
 */

public class RecommendationEngine {
    public static void main(String[] args) {
        // STEP 2: create a SparkConf object
        SparkConf conf = new SparkConf().setAppName("SparkDemo") ;

        // STEP 3: create a Java Spark context
        JavaSparkContext sparkContext = new JavaSparkContext(conf) ;

        if(args.length<1){
            throw new RuntimeException();
        }

        String pathRatings = "ratings.dat";
        String pathMovies = "movies.dat";
        JavaRDD<String> rawRatings = sparkContext.textFile(pathRatings).repartition(2);
        JavaRDD<String> rawMovies = sparkContext.textFile(pathMovies);

        JavaRDD<Tuple3<Integer, Integer, Float>> ratingsRDD = rawRatings.map(
                p -> {
                    String[] saux = p.split("::");
                    return new Tuple3<>(Integer.parseInt(saux[0]), Integer.parseInt(saux[1]), Float.parseFloat(saux[2]));
                }
        ).cache();

        JavaPairRDD<Integer, String> moviesRDD = rawMovies.mapToPair(
                p -> {
                    String[] saux = p.split("::");
                    return new Tuple2<>(Integer.parseInt(saux[0]), saux[1]);
                }
        ).cache();

        JavaPairRDD<Integer, Tuple2<Float, Integer>> movieIDsWithRatingsRDD = ratingsRDD
                .mapToPair( p -> new Tuple2<>(p._2(), new Tuple2<>(p._3(),1)))
                .reduceByKey((Tuple2<Float, Integer> a, Tuple2<Float, Integer> b) ->
                        new Tuple2<>(a._1 + b._1, a._2 + b._2));

        JavaPairRDD<Integer, Tuple2<Integer, Float>> movieIDsWithAvgRatingsRDD = movieIDsWithRatingsRDD
                .mapToPair( p -> new Tuple2<>(p._1(), new Tuple2<>((p._2._2), (p._2._1/p._2._2))));

        JavaRDD<Tuple3<Float, String, Integer>> movieNameWithAvgRatingsRDD = (movieIDsWithAvgRatingsRDD.join(moviesRDD)
                .map(p -> new Tuple3<Float, String, Integer>(p._2._1._2, p._2._2, p._2._1._1)));


        JavaRDD<Tuple3<Float, String, Integer>> movieLimitedAndSortedByRatingRDD = movieNameWithAvgRatingsRDD
                .filter(p -> p._3() > 500)
                .sortBy(p -> p._1() + "" + p._2(), false, 1);

        List<Tuple3<Float, String, Integer>> list = movieLimitedAndSortedByRatingRDD.take(20);
        System.out.println("Movies with highest ratings: ");

        for(Tuple3<Float, String, Integer> t : list){
            System.out.println(t._1()+ " " + t._2() + " " + t._3());
        }
    }
}
